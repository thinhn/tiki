<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>Tiki</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

        <?php $this->load->view('common/head'); ?>
    </head>
    <body>
        <section>
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="wrapper-page">
                            <div class="m-t-40 card-box">
                				<?php echo validation_errors('<div class="alert alert-danger">', '</div>'); ?>
                                <?php
                                    if($this->session->flashdata('item')): 
                                        $message = $this->session->flashdata('item');
                                ?>
                                    <div class="alert alert-<?= $message['class'] ?>" ><?php echo $message['message']; ?></div>
                                <?php endif; ?>
                				<a href="/logout" class="btn btn-xs btn-warning pull-right" type="submit">Log out</a>
                				<form class="form-horizontal" action="/" method="POST">
	                                <div class="account-content">
	                                    <div class="text-center m-b-20">
	                                        <h3 class="expired-title"><?= $username ?></h3>
	                                    </div>
	                                    <form class="form-horizontal" action="#">
                                            <input name="username" type="hidden" value="<?= $username ?>">
	                                        <div class="form-group m-b-20">
	                                            <div class="col-xs-12">
	                                                <label for="password">Password</label>
	                                                <input name="old_password" class="form-control" type="password" placeholder="Enter your old password">
	                                            </div>
	                                        </div>
	                                        <div class="form-group m-b-20">
	                                            <div class="col-xs-12">
	                                                <label for="password">New Password</label>
	                                                <input name="new_password" class="form-control" type="password" placeholder="Enter your new password">
	                                            </div>
	                                        </div>
	                                        <div class="form-group m-b-20">
	                                            <div class="col-xs-12">
	                                                <label for="password">Confirm Password</label>
	                                                <input name="confirm_password" class="form-control" type="password" placeholder="Enter your confirm password">
	                                            </div>
	                                        </div>
	                                        <div class="form-group account-btn text-center m-t-10">
	                                            <div class="col-xs-12">
	                                                <button class="btn btn-lg btn-primary btn-block" type="submit">Change password</button>
	                                            </div>
	                                        </div>
	                                    </form>
	                                    <div class="clearfix"></div>
	                                </div>
                            	</form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <?php $this->load->view('common/script'); ?>
    </body>
</html>