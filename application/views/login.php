<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>Tiki</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

        <?php $this->load->view('common/head'); ?>
    </head>
    <body>
        <section>
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="wrapper-page">
                            <div class="m-t-40 card-box">
                                <div class="text-center">
                                    <h2 class="text-uppercase m-t-0 m-b-30">
                                        <a href="javacript: void(0)" class="text-success">
                                            Login
                                        </a>
                                    </h2>
                                </div>
                                <div class="account-content">
                                	<?php echo validation_errors('<div class="alert alert-danger">', '</div>'); ?>
                                	<?php
	                                    if($this->session->flashdata('item')): 
	                                        $message = $this->session->flashdata('item');
	                                ?>
	                                    <div class="alert alert-<?= $message['class'] ?>" ><?php echo $message['message']; ?></div>
	                                <?php endif; ?>

                                    <form class="form-horizontal" action="/user/login" method="POST">
                                        <div class="form-group m-b-20">
                                            <div class="col-xs-12">
                                                <label for="emailaddress">Username</label>
                                                <input class="form-control" name="username" type="text" placeholder="Enter your username">
                                            </div>
                                        </div>
                                        <div class="form-group m-b-20">
                                            <div class="col-xs-12">
                                                <label for="password">Password</label>
                                                <input class="form-control" name="password" type="password" placeholder="Enter your password">
                                            </div>
                                        </div>
                                        <div class="form-group account-btn text-center m-t-10">
                                            <div class="col-xs-12">
                                                <button class="btn btn-lg btn-primary btn-block" type="submit" name="login">Sign In</button>
                                            </div>
                                        </div>
                                    </form>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="row m-t-50">
                                <div class="col-sm-12 text-center">
                                    <p class="text-muted">Don't have an account? <a href="/register" class="text-dark m-l-5">Sign Up</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </body>
</html>