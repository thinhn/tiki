<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>Tiki</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

        <?php $this->load->view('common/head'); ?>
    </head>
    <body>
        <section>
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="wrapper-page">
                            <div class="m-t-40 card-box">
                                <div class="text-center">
                                    <h2 class="text-uppercase m-t-0 m-b-30">
                                        <a href="javacript: void(0)" class="text-success">
                                            Register
                                        </a>
                                    </h2>
                                </div>
                                <div class="account-content">
                                	<?php echo validation_errors('<div class="alert alert-danger">', '</div>'); ?>
                                    <?php
                                        if($this->session->flashdata('item')): 
                                            $message = $this->session->flashdata('item');
                                    ?>
                                        <div class="alert alert-<?= $message['class'] ?>" ><?php echo $message['message']; ?></div>
                                    <?php endif; ?>
                                    <form class="form-horizontal" action="/user/register" method="POST">
                                        <div class="form-group m-b-20">
                                            <div class="col-xs-12">
                                                <label for="username">Username <span style="color: red">*</span></label>
                                                <input name="username" class="form-control" type="text" placeholder="Enter your username">
                                            </div>
                                        </div>
                                        <div class="form-group m-b-20">
                                            <div class="col-xs-12">
                                                <label for="password">Password <span style="color: red">*</span></label>
                                                <input name="password" class="form-control" type="password" placeholder="Enter your password">
                                            </div>
                                        </div>
                                        <div class="form-group m-b-20">
                                            <div class="col-xs-12">
                                                <label for="password">Confirm Password</label>
                                                <input name="confirm_password" class="form-control" type="password" placeholder="Enter your confirm password">
                                            </div>
                                        </div>
                                        <div class="form-group account-btn text-center m-t-10">
                                            <div class="col-xs-12">
                                                <button name= "register" class="btn btn-lg btn-primary btn-block" type="submit">Sign Up</button>
                                            </div>
                                        </div>
                                    </form>
                                    <div class="clearfix"></div>
                                </div>
                                <span style="color: red">*</span>
                                <ul>
                                    <li>The username and password must not contain any whitespace</li>
                                    <li>The username must be at a->z, A->Z, 0->9, _</li>
                                    <li>The username and password must be at least 6 characters long</li>
                                    <li>The password must contain at least one uppercase and at least one lowercase letter</li>
                                    <li>The password must have at least one digit and symbol</li>
                                </ul>
                            </div>
                            <div class="row m-t-50">
                                <div class="col-sm-12 text-center">
                                    <p class="text-muted">Already have an account?  <a href="/login" class="text-dark m-l-5">Sign In</a></p>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </section>
        <?php $this->load->view('common/script'); ?>
    </body>
</html>