<?php
class PasswordManager extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public $username;
	public $encrypted_password;

	protected function encrypt($str)
	{
		return md5($str);
	}

	protected function verifyPassword($str)
	{
		$verify_password = $this->encrypt($str);
		if ($verify_password == $this->encrypted_password) return true;
		return false;
	}

    public function validatePassword($password) 
	{
		// check whitespace
		if (preg_match('/\s/',$password)) {
			$this->form_validation->set_message('validatePassword', 'The {field} field must not contain any whitespace');
			return false;
		}
		// check length string 6 > 
		if (strlen($password) <= 6) {
			$this->form_validation->set_message('validatePassword', 'The {field} field must be at least 6 characters long');
			return false;
		}
		// check uppercase and lowercase
		$regex = "/^(?=.*[a-z])(?=.*[A-Z]).*$/";
		if (!preg_match($regex, $password)) {
			$this->form_validation->set_message('validatePassword', 'The {field} field must contain at least one uppercase and at least one lowercase letter');
			return false;
		}

		// check digit and symbol
		$regex = "/^(?=.*\d)(?=.*[!@#$%^&*]).*$/";
		if (!preg_match($regex, $password)) {
			$this->form_validation->set_message('validatePassword', 'The {field} field must have at least one digit and symbol');
			return false;
		}

		return true;
	}

	public function validateUsername($username)
	{
		// check whitespace
		if (preg_match('/\s/',$username)) {
			$this->form_validation->set_message('validateUsername', 'The {field} field must not contain any whitespace');
			return false;
		}
		// check length string 6 > 
		if (strlen($username) <= 6) {
			$this->form_validation->set_message('validateUsername', 'The {field} field must be at least 6 characters long');
			return false;
		}

		if ( !preg_match('/^[A-Za-z0-9_]+$/', $username)) {
            $this->form_validation->set_message('validateUsername', 'The {field} field must be at a->z, A->Z, 0->9, _');
			return false;
        }
        return true;
	}

	public function setNewPassword($password) 
	{
		if ($this->validate($password)) {
			
		}
	}

	public function checkConfirmPassword($password)
	{	
		if ($this->encrypt($password) != $this->encrypted_password) {
			$this->form_validation->set_message('checkConfirmPassword', 'The {field} field does not match');
			return false;
		}
		return true;
	}
}
