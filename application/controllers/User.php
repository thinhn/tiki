<?php

class User extends PasswordManager   
{
	public function __constructor()
	{

	}

    public function index()
    {
    	// $this->M_user->getUser();
        if (!empty($this->session->user_login)) {
            $params['username'] = $this->session->user_login;
            $this->changePassword();
        } else {
            return redirect('login');
        }
        $this->load->view('home', $params);
    }

    public function login()
    {
        $this->session->user_login = '';
        $username = $this->input->post('username');

        $password = $this->input->post('password');

        $this->form_validation->set_rules('username', 'Username', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');

        if ($this->form_validation->run() == true ) {
        
            $data = $this->checkExistUser($username);
            if (!$data) {
                $this->session->set_flashdata('item', array('message' => 'Username or Password incorrect!','class' => 'danger'));
                return redirect('login');
            }
            $this->encrypted_password = $data[1];
            if ($this->verifyPassword($password)) {
                $this->session->user_login = $username;
                return redirect('/');
            }
            $this->session->set_flashdata('item', array('message' => 'Username or Password incorrect!','class' => 'danger'));
            return redirect('login');
        }
        $this->load->view('login');
    }

    public function register()
    {
        $this->session->user_login = '';
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        $confirm_password = $this->input->post('confirm_password');

        $this->form_validation->set_rules('username', 'Username', 'required|callback_validateUsername');
        $this->form_validation->set_rules('password', 'Password', 'required|callback_validatePassword');
        $this->encrypted_password = $this->encrypt($password);
        $this->form_validation->set_rules('confirm_password', 'Confirm Password', 'required|callback_checkConfirmPassword');
        if ($this->form_validation->run() == true ) {
            $this->username = $username;
            if (!$this->checkExistUser($this->username)) {
                $this->saveUser($this->username.'|'.$this->encrypted_password);
                $this->session->set_flashdata('item', array('message' => 'User created successfully','class' => 'success'));
            } else {
                $this->session->set_flashdata('item', array('message' => 'Username '.$this->username.' exist!','class' => 'danger'));
            }
            return redirect('register');
        }
        $this->load->view('register');
    }

    public function logout() 
    {
        $this->session->user_login = '';
        return redirect('/login');
    }

    private function changePassword()
    {
        $username = $this->input->post('username');
        $password = $this->input->post('old_password');
        $new_password = $this->input->post('new_password');
        $confirm_password = $this->input->post('confirm_password');

        $this->form_validation->set_rules('old_password', 'Password', 'required');
        $this->form_validation->set_rules('new_password', 'New Password', 'required|callback_validatePassword');
        $this->form_validation->set_rules('confirm_password', 'Confirm Password', 'required|callback_validatePassword');

        if ($this->form_validation->run() == true) {
            $data = $this->checkExistUser($username);
            if (!$data) return redirect('login');
            $this->encrypted_password = $data[1];
            if ($this->verifyPassword($password)) {
                $this->updateUser($username, $this->encrypt($new_password));
                $this->session->set_flashdata('item', array('message' => 'Password changed!','class' => 'success'));
                return redirect('/');
            }
            $this->session->set_flashdata('item', array('message' => 'Password incorrect!','class' => 'danger'));
            return redirect('/');
        }
    }

    private function checkExistUser($username)
    {
        $file = FCPATH.'/data/password.txt';
        $handle = fopen($file, "r");
        if ($handle) {
            while (($line = fgets($handle)) !== false) {
                $data = explode('|', $line);
                if ($data[0] == $username) {
                    return $data;
                }
            }
            fclose($handle);
        }
        return false;
    }

    private function saveUser($str)
    {
        $file = FCPATH.'/data/password.txt';
        $handle = fopen($file, 'a');
        fwrite($handle, $str.'|'.PHP_EOL);
        fclose($handle);
    }

    private function updateUser($username, $encrypted_password)
    {
        $file = FCPATH.'/data/password.txt';
        $content = file($file);
        foreach ($content as $line_num => $line) {
            $data = explode('|', $line);
            if ($data[0] == $username) {
                $str = $username.'|'.$encrypted_password.'|'.PHP_EOL;
                $content[$line_num] = $str;
            }
        }
        file_put_contents($file, $content);
    }

}